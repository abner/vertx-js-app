"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var options_1 = require("@reactiverse/reactive-pg-client/options");
exports.DEFAULT_DB_CONFIG = {
    host: 'localhost',
    user: 'postgres',
    password: 'postgres',
    port: 5432,
    database: 'postgres',
};
function getDbConfig(paramsDefault) {
    var dbParams = __assign({}, exports.DEFAULT_DB_CONFIG, paramsDefault);
    console.log('PARAMS', dbParams);
    var options = new options_1.PgPoolOptions();
    Object.keys(dbParams).forEach(function (dbConfigKey) {
        console.log(dbConfigKey, process.env["DB_" + dbConfigKey.toUpperCase()] || dbParams[dbConfigKey]);
        options["set" + exports.capitalize(dbConfigKey)](process.env["DB_" + dbConfigKey.toUpperCase()] || dbParams[dbConfigKey]);
    });
    debugger;
    console.log('USER', options.toJson());
    return options;
}
exports.getDbConfig = getDbConfig;
/**
 * string capitalization - first letter - capital, other - lowercase.
 * @param {String} word - Word or sentence.
 */
exports.capitalize = function (word) {
    return "" + word.slice(0, 1).toUpperCase() + word.slice(1).toLowerCase();
};
