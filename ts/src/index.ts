import { HttpServerOptions } from '@vertx/core/options';
import { PostgresDBClient } from './db-client';

const httpOptions = new HttpServerOptions();
(httpOptions as any).setPort(8080); // typings is missing the set methods and fails on graalVM

const httpServer = vertx.createHttpServer(httpOptions);

const SQL = 'SELECT cpf, exercicio from declaracao';

const dbConfig = {
  user: 'irpfonline',
  password: 'irpfonline',
  database: 'irpfonline',
};

const postgresClient = new PostgresDBClient(vertx, dbConfig);

httpServer
  .requestHandler(async r => {
    const rs = await postgresClient.preparedQuery(SQL);
    const item = rs.iterator().next();
    const result = {
      cpf: item.getString('cpf'),
      exercicio: item.getInteger('exercicio'),
    };
    r.response().end(JSON.stringify(result));
  })
  .listen();
