import { PgPoolOptions } from '@reactiverse/reactive-pg-client/options';

export interface DbParams {
  host: string;
  database: string;
  port: number;
  user: string;
  password: string;
}

export const DEFAULT_DB_CONFIG: DbParams = {
  host: 'localhost',
  user: 'postgres',
  password: 'postgres',
  port: 5432,
  database: 'postgres',
};

export function getDbConfig(paramsDefault: Partial<DbParams>): PgPoolOptions {
  const dbParams = {
    ...DEFAULT_DB_CONFIG,
    ...paramsDefault,
  };
  console.log('PARAMS', dbParams);
  const options = new PgPoolOptions();
  Object.keys(dbParams).forEach(dbConfigKey => {
    console.log(dbConfigKey, process.env[`DB_${dbConfigKey.toUpperCase()}`] || dbParams[dbConfigKey]);
    options[`set${capitalize(dbConfigKey)}`](process.env[`DB_${dbConfigKey.toUpperCase()}`] || dbParams[dbConfigKey]);
  });
  debugger;
  console.log('USER', (options as any).toJson());
  return options;
}

/**
 * string capitalization - first letter - capital, other - lowercase.
 * @param {String} word - Word or sentence.
 */
export const capitalize = word => {
  return `${word.slice(0, 1).toUpperCase()}${word.slice(1).toLowerCase()}`;
};
