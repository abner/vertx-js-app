import { Tuple, PgRowSet, PgClient } from '@reactiverse/reactive-pg-client';
import * as util from 'util';
import { Vertx } from '@vertx/core';
import { getDbConfig, DbParams } from './db-config';

/**
 * Class to wrap postgres conectivity using the reactive-pg-client,
 * exposing its methods using Promisified alternatives to
 * allow use async/await targetting Nashorn (Java 8) or GraalVM
 */
export class PostgresDBClient {
  private client: any;
  constructor(vertx: Vertx, args: Partial<DbParams>) {
    const options = getDbConfig(args);
    console.log('OPTIONS', options.user, options.host, options.password, options.database);
    const _client = PgClient.pool(vertx, options);
    this.client = util.promisify(_client as any);
  }

  async preparedQuery(sql: string, params?: Tuple | null): Promise<PgRowSet> {
    if (params) {
      return this.client.preparedQuery(sql, params);
    } else {
      return this.client.preparedQuery(sql);
    }
  }
}
