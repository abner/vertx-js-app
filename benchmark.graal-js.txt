ab -n 2000 -c 80 http://localhost:8080/
This is ApacheBench, Version 2.3 <$Revision: 1807734 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)
Completed 200 requests
Completed 400 requests
Completed 600 requests
Completed 800 requests
Completed 1000 requests
Completed 1200 requests
Completed 1400 requests
Completed 1600 requests
Completed 1800 requests
Completed 2000 requests
Finished 2000 requests


Server Software:        
Server Hostname:        localhost
Server Port:            8080

Document Path:          /
Document Length:        38 bytes

Concurrency Level:      80
Time taken for tests:   0.442 seconds
Complete requests:      2000
Failed requests:        0
Total transferred:      154000 bytes
HTML transferred:       76000 bytes
Requests per second:    4521.92 [#/sec] (mean)
Time per request:       17.692 [ms] (mean)
Time per request:       0.221 [ms] (mean, across all concurrent requests)
Transfer rate:          340.03 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    1   1.4      1       8
Processing:     3   16   4.6     16      38
Waiting:        3   15   4.4     15      34
Total:          7   17   4.3     17      39

Percentage of the requests served within a certain time (ms)
  50%     17
  66%     19
  75%     20
  80%     20
  90%     23
  95%     25
  98%     28
  99%     31
 100%     39 (longest request)
