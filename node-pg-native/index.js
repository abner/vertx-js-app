const PgAsync = require('pg-async').default;
const SQL = PgAsync.SQL;

console.log(PgAsync);

const user = 'irpfonline';
const password = 'irpfonline';
const host = 'localhost';
const port = 5432;
const database = 'irpfonline';
const max = 50;
// using connection string
const pgAsync = new PgAsync({
    user,
    password,
    host,
    port,
    database,
    max
}, 'native');

const declaracoesTable = 'declaracao';

const sqlDeclaracaoByCpf = (cpf) => SQL `
  select cpf, exercicio
  from $ID${declaracoesTable}
  where cpf = ${cpf}
`;

async function getDeclaracoes() {
    const declaracoes = await pgAsync.rows(sqlDeclaracaoByCpf('34030566278'));
    // console.log('Declaracoes obtidas', declaracoes);
    return declaracoes;
}


const http = require('http');

const server = http.createServer();
server.on('request', async (req, res) => {
    const data = await getDeclaracoes();
    // console.log(req.url);
    // console.log(data);
    res.end(JSON.stringify(data[0]));
});

server.listen(9000);