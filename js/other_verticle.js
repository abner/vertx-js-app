vertx.setPeriodic(2000, function (r) {
    console.log('Running periodic fuction', r);
});

vertx.setPeriodic(1000, function (r) {
    console.log('Running periodic fuction', r);
});

vertx.executeBlocking(function (future) {
    console.log('Running blocking block');
    future.complete();
}, function (r) {
    console.log('After running blocking function');
});