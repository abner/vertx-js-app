import {
    Router
} from '@vertx/web';

const BackendRegistries = Java.type('io.vertx.micrometer.backends.BackendRegistries');

const registry = BackendRegistries.getDefaultNow();

const app = Router.router(vertx);

    // Setup a route for metrics
app.route("/metrics").handler(function(ctx) {
  const response = registry.scrape();
  ctx.response().end(response);
})

vertx.createHttpServer().requestHandler(function (req) {
     app.accept(req)
}).listen(8081);


