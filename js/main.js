/// <reference types="@vertx/core/runtime" />


// @ts-check
const util = require('util');

import {
    Router
} from '@vertx/web';

import {
    PgClient,
    Tuple
} from '@reactiverse/reactive-pg-client';
import {
    PgPoolOptions
} from '@reactiverse/reactive-pg-client/options';

import {  MetricsService } from "@vertx/micrometer-metrics";
import { MicrometerMetricsOptions, VertxPrometheusOptions } from "@vertx/micrometer-metrics/options";

const BackendRegistries = Java.type('io.vertx.micrometer.backends.BackendRegistries');

const SELECT_WORLD = "SELECT cpf, exercicio from declaracao"; // where cpf=$1";

const app = Router.router(vertx);

console.log('Deployment ID: ', vertx.getOrCreateContext().deploymentID())

var client = PgClient.pool(
    vertx,
    new PgPoolOptions()
    .setUser('irpfonline')
    .setPassword('irpfonline')
    .setDatabase('irpfonline'));

// deploy de outro vértice
vertx.deployVerticle('js:./other_verticle', function (r) {
    console.log('Started OtherVerticle', r.failed());
});

app.get('/').handler( // Step #2
    function (ctx) {
        // vert.x promise
        var postgres = util.promisify(client);

        postgres
            .preparedQuery(SELECT_WORLD /*, Tuple.of(1)*/ )
            .then(function (rs) {
                var resultSet = rs.iterator();

                if (!resultSet.hasNext()) {
                    ctx.fail(404);
                } else {
                    let row = resultSet.next();
                    ctx.response()
                        .putHeader("Content-Type", "application/json")
                        .end(JSON.stringify({
                            cpf: row.getString(0),
                            exercicio: row.getString(1)
                        }));
                }
            })
            .catch(function (e) {
                ctx.fail(e);
            });
    });

const options = new MicrometerMetricsOptions()
    .setPrometheusOptions(new VertxPrometheusOptions().setEnabled(true))
    .setEnabled(true);    
var registry = Java.type("io.vertx.micrometer.backends.BackendRegistries").getDefaultNow();

// Setup a route for metrics
app.route("/metrics").handler(function(ctx) {
    var registry = vertx.getOrCreateContext().get("registry");
    var response = registry.scrape();
    ctx.response().end(response);
});
// async ctx => {
// // vert.x promise
// let postgres = util.promisify(client);

// try {
//     let rs = await postgres.preparedQuery(SELECT_WORLD, Tuple.of(1));
//     let resultSet = rs.iterator();

//     if (!resultSet.hasNext()) {
//         ctx.fail(404);
//     } else {
//         let row = resultSet.next();
//         ctx.response()
//             .putHeader("Content-Type", "application/json")
//             .end(JSON.stringify({
//                 id: row.getInteger(0),
//                 randomNumber: row.getInteger(1)
//             }));
//     }
// } catch (e) {
//     ctx.fail(e);
// }
//});

vertx
    .createHttpServer()
    .requestHandler(function (req) {
        app.accept(req)
    })
    .listen(8080);

console.log('Server listening at: http://localhost:8080/');