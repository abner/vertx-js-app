/// <reference types="@vertx/core/runtime" />


// @ts-check
const util = require('util');

import {
    Router
} from '@vertx/web';

import {
    Vertx
} from '@vertx/core';
import {
    VertxOptions,
    HttpServerOptions
} from '@vertx/core/options';

import {
    PgClient,
    Tuple
} from '@reactiverse/reactive-pg-client';
import {
    PgPoolOptions
} from '@reactiverse/reactive-pg-client/options';

// import {  MetricsService } from "@vertx/micrometer-metrics";
// import {  VertxJmxMetricsOptions, MicrometerMetricsOptions, VertxPrometheusOptions } from "@vertx/micrometer-metrics/options";

// const BackendRegistries = Java.type('io.vertx.micrometer.backends.BackendRegistries');

// const options = new MicrometerMetricsOptions();

// options.setJmxMetricsOptions(new VertxJmxMetricsOptions().setEnabled(true))

// const vertxPrometheusOptions = new VertxPrometheusOptions();

// vertxPrometheusOptions.enabled = true;
// vertxPrometheusOptions.startEmbeddedServer = true;
// vertxPrometheusOptions.embeddedServerEndpoint = 'metrics/vertx';

// const httpServerOption = new HttpServerOptions();
// httpServerOption.port = 8081;
// vertxPrometheusOptions.embeddedServerOptions = httpServerOption;

// options.prometheusOptions = vertxPrometheusOptions;

// options.setEnabled(true);    

// console.log('PROMETHEUS OPTIONS', options.prometheusOptions.enabled);

// console.log('OPT', options.registryName);

// console.log('OPTIONS', VertxPrometheusOptions, MicrometerMetricsOptions, options);

// let vertx = Vertx.vertx(new VertxOptions().setMetricsOptions(options));

// const r = BackendRegistries.setupBackend(vertx, options)

// console.log('REGISTRIES: ', typeof registry);




const SELECT_WORLD = "SELECT cpf, exercicio from declaracao"; // where cpf=$1";

const app = Router.router(vertx);

console.log('Deployment ID: ', vertx.getOrCreateContext().deploymentID())

var client = PgClient.pool(
    vertx,
    new PgPoolOptions()
    .setUser('irpfonline')
    .setPassword('irpfonline')
    .setDatabase('irpfonline'));

// deploy de outro vértice
vertx.deployVerticle('js:./other_verticle', function (r) {
    console.log('Started OtherVerticle', r.failed());
});

app.get('/').handler( // Step #2
    function (ctx) {
        // vert.x promise
        var postgres = util.promisify(client);

        postgres
            .preparedQuery(SELECT_WORLD /*, Tuple.of(1)*/ )
            .then(function (rs) {
                var resultSet = rs.iterator();

                if (!resultSet.hasNext()) {
                    ctx.fail(404);
                } else {
                    let row = resultSet.next();
                    ctx.response()
                        .putHeader("Content-Type", "application/json")
                        .end(JSON.stringify({
                            cpf: row.getString(0),
                            exercicio: row.getString(1)
                        }));
                }
            })
            .catch(function (e) {
                ctx.fail(e);
            });
    });


/// var registry = Java.type("io.vertx.micrometer.backends.BackendRegistries").getDefaultNow();

// Setup a route for metrics

const SharedMetricRegistries = Java.type("com.codahale.metrics.SharedMetricRegistries");
const CollectorRegistry = Java.type("io.prometheus.client.CollectorRegistry");
const DropwizardExports = Java.type("io.prometheus.client.dropwizard.DropwizardExports");
const MetricsHandler = Java.type("io.prometheus.client.vertx.MetricsHandler");

const metricRegistry = SharedMetricRegistries.getDefault(); // SharedMetricRegistries.getOrCreate("default");
CollectorRegistry.defaultRegistry.register(new DropwizardExports(metricRegistry));

app.get("/metrics").handler(new MetricsHandler(CollectorRegistry.defaultRegistry));

// async ctx => {
// // vert.x promise
// let postgres = util.promisify(client);

// try {
//     let rs = await postgres.preparedQuery(SELECT_WORLD, Tuple.of(1));
//     let resultSet = rs.iterator();

//     if (!resultSet.hasNext()) {
//         ctx.fail(404);
//     } else {
//         let row = resultSet.next();
//         ctx.response()
//             .putHeader("Content-Type", "application/json")
//             .end(JSON.stringify({
//                 id: row.getInteger(0),
//                 randomNumber: row.getInteger(1)
//             }));
//     }
// } catch (e) {
//     ctx.fail(e);
// }
//});

vertx
    .createHttpServer()
    .requestHandler(function (req) {
        app.accept(req)
    })
    .listen(8080);

console.log('Server listening at: http://localhost:8080/');