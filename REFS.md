## Idéias

### 1. Implementar driver posgres-reactive para TypeORM

https://github.com/typeorm/typeorm/blob/master/src/driver/sqlite/SqliteDriver.ts

### 2. JOOQ para lightweight SQL mapping (Fazer avaliação de performance)

- https://github.com/jklingsporn/vertx-jooq
- https://mvnrepository.com/artifact/io.github.jklingsporn/vertx-jooq-rx-async/4.0.0-BETA2

### 3. Exemplo de Server customizado para Redpipe

- Implementar um exemplo utilizando o Driver postgres-reactive e também habilitando as métricas

https://github.com/FroMage/redpipe/blob/4ffe77440346c6861ed5b6f41cf4e45520f83e39/redpipe-example-wiki-keycloak-jooq/src/main/java/net/redpipe/example/wiki/keycloakJooq/WikiServer.java

## Referências

Projeto RedPipe com Gradle: https://github.com/aesteve/redpipe-todobackend-mongo/blob/master/build.gradle

### Lifecycle classe Resource Jax-RS

https://howtodoinjava.com/jaxb/life-cycle-of-jax-rs-resource-class/

Sempre cria uma nova instância do Resource

### Explorar Exemplo com Autenticação / Autorização utilizando Redpipe

https://github.com/FroMage/redpipe/blob/0ab367019eeb00b11a54363d8ace58eec3abaeb0/redpipe-engine/src/test/java/net/redpipe/engine/TestResourceRxJava1.java
