package io.abner.redpipe;

public class DataObject {
    private String cpf;
    private int exercicio;

    /**
     * @return the cpf
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * @return the exercicio
     */
    public int getExercicio() {
        return exercicio;
    }

    /**
     * @param exercicio the exercicio to set
     */
    public void setExercicio(int exercicio) {
        this.exercicio = exercicio;
    }

    /**
     * @param cpf the cpf to set
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

}