package io.abner.redpipe;

import static net.redpipe.router.Router.getURI;

import java.io.IOException;
import java.io.Writer;
import java.net.URI;
// import java.util.HashSet;
// import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

//import io.prometheus.client.CollectorRegistry;
//import io.prometheus.client.exporter.common.TextFormat;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;
//import io.vertx.micrometer.backends.BackendRegistries;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.core.buffer.Buffer;
import io.vertx.reactivex.core.http.HttpServerRequest;
import io.vertx.reactivex.ext.web.RoutingContext;
import io.vertx.reactivex.ext.web.client.HttpResponse;
import io.vertx.reactivex.ext.web.client.WebClient;
import net.redpipe.engine.core.AppGlobals;
import net.redpipe.engine.db.SQLUtil;
import net.redpipe.fibers.Fibers;

@Path("/")
public class HelloResource {

    static DbService dbService = new DbService();
    static boolean dbServiceInitialized = false;

    private static class BufferWriter extends Writer {

        private final Buffer buffer = Buffer.buffer();

        @Override
        public void write(char[] cbuf, int off, int len) throws IOException {
            buffer.appendString(new String(cbuf, off, len));
        }

        @Override
        public void flush() throws IOException {
            // NO-OP
        }

        @Override
        public void close() throws IOException {
            // NO-OP
        }

        Buffer getBuffer() {
            return buffer;
        }
    }

    // public HelloResource(@Context Vertx vertx) {
    // System.out.println("VERTX: " + vertx.toString());
    // }

    public HelloResource(@Context Vertx vertx, @Context AppGlobals globals) {
        // System.out.println("CONFIG: " + globals.getConfig().encodePrettily());
        // globals.getDbConnection().subscribe(conn -> {
        // conn.rxQuery("select conteudo, cpf from declaracao where
        // cpf='34030566278'").subscribe(rs -> {
        // System.out.println(rs.toJson().encodePrettily());
        // });
        // System.out.println("CONFIG: " + conn.toString());
        // });
        if (!HelloResource.dbServiceInitialized) {
            DbService.setup(vertx);
            dbServiceInitialized = true;
        }
    }

    // @GET
    // @Path("metrics")
    // public String getMetrics(@Context io.vertx.ext.web.RoutingContext ctx) {

    // CollectorRegistry registry = CollectorRegistry.defaultRegistry;

    // try {
    // final BufferWriter writer = new BufferWriter();
    // TextFormat.write004(writer,
    // registry.filteredMetricFamilySamples(parse(ctx.request())));
    // // ctx.response().setStatusCode(200).putHeader("Content-Type",
    // // TextFormat.CONTENT_TYPE_004)
    // // .end(writer.getBuffer().getDelegate());
    // return writer.getBuffer().toString();
    // } catch (IOException e) {
    // ctx.fail(e);
    // }
    // }

    // private Set<String> parse(io.vertx.core.http.HttpServerRequest request) {
    // return new HashSet<>(request.params().getAll("name[]"));
    // }

    @GET
    @Path("pessoaFisica")
    public Single<JsonObject> getPessoaFisica(@Context Vertx vertx) throws Exception {

        return Fibers.fiber(() -> {
            // return Fibers.await(dbService.getPessoaFisica());
            JsonObject jsonPessoa = Fibers.await(dbService.getPessoaFisica());
            System.out.println(jsonPessoa.encodePrettily());
            return jsonPessoa;
        });
    }

    @GET
    @Path("pessoaFisicaJDBC")
    public Single<Response> getPessoaFisica() throws Exception {
        return SQLUtil
                .doInConnection(conn -> conn.rxQuery("select cpf, exercicio from declaracao where cpf='34030566278'")
                        .map(res -> Response.status(Status.OK).entity(res.getRows().get(0)).build()));
    }

    @GET
    public String hello() {
        return "Hello World";
    }

    @Path("reactive")
    @GET
    public Single<String> helloReactive() {
        return Single.just("Hello Reactive World");
    }

    @Path("fiber")
    @GET
    public Single<String> helloFiber(@Context Vertx vertx, @Context UriInfo uriInfo) {
        return Fibers.fiber(() -> {
            String hello1 = Fibers.await(get(vertx, getURI(HelloResource::hello)));
            String hello2 = Fibers.await(get(vertx, getURI(HelloResource::helloReactive)));
            return hello1 + "\n" + hello2;
        });
    }

    private Single<String> get(Vertx vertx, URI uri) {
        WebClient client = WebClient.create(vertx);
        Single<HttpResponse<Buffer>> responseHandler = client.get(uri.getPort(), uri.getHost(), uri.getPath()).rxSend();
        return responseHandler.map(response -> response.body().toString()).doAfterTerminate(() -> client.close());
    }

}