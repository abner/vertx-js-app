package io.abner.redpipe;

import net.redpipe.engine.core.Server;

public class Main {
    public static void main(String[] args) {
        DataObject d1 = new DataObject();
        d1.setCpf("80129498572");
        d1.setExercicio(2017);
        System.out.println(d1.toString());
        new Server().start(HelloResource.class).subscribe(() -> System.err.println("Server started"),
                Throwable::printStackTrace);
    }
}