package io.abner.redpipe;

import com.github.fromage.quasi.fibers.Suspendable;

// import io.reactiverse.pgclient.PgPool;
import io.reactiverse.pgclient.PgPoolOptions;
// import io.reactiverse.pgclient.PgRowSet;
import io.reactiverse.rxjava.pgclient.PgClient;
import io.reactiverse.rxjava.pgclient.PgPool;
import io.reactiverse.rxjava.pgclient.PgRowSet;
import io.reactiverse.rxjava.pgclient.Row;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.Vertx;

public class DbService {
    private static PgPool client;
    private static PgPoolOptions options = new PgPoolOptions().setPort(5432).setHost("localhost")
            .setDatabase("irpfonline").setUser("irpfonline").setPassword("irpfonline").setMaxSize(5);

    public static synchronized void setup(Vertx vertx) {
        if (client == null) {
            client = PgClient.pool(io.vertx.rxjava.core.Vertx.newInstance(vertx.getDelegate()), options);
        }
    }

    public DbService() {

    }

    @Suspendable
    public Single<JsonObject> getPessoaFisica() {
        return this.query("SELECT cpf, exercicio from declaracao").map(result -> {
            Row row = result.iterator().next();
            return new JsonObject().put("cpf", row.getString("cpf")).put("exercicio", row.getInteger("exercicio"));
        });
    }

    @Suspendable
    private Single<PgRowSet> query(String sql) {
        return Single.create(emitter -> {
            client.query(sql, h -> {
                if (h.succeeded()) {
                    emitter.onSuccess(h.result());
                } else {
                    emitter.onError(h.cause());
                }
            });
        });
    }
}