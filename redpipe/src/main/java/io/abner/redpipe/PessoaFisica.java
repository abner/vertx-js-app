
package io.abner.redpipe;

public class PessoaFisica {
    private String cpf;
    private String exercicio;

    public PessoaFisica() {
    }

    public PessoaFisica(String cpf, String exercicio) {
        this.setCpf(cpf);
        this.setExercicio(exercicio);
    }

    /**
     * @return the exercicio
     */
    public String getExercicio() {
        return exercicio;
    }

    /**
     * @param exercicio the exercicio to set
     */
    public void setExercicio(String exercicio) {
        this.exercicio = exercicio;
    }

    /**
     * @return the cpf
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * @param cpf the cpf to set
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

}